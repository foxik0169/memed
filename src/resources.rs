use std::{ fs::{FileType, Metadata}, path::Path};

use sdl2::{render::TextureCreator, ttf::Font, image::LoadTexture, video::WindowContext};
use stretch::{style::Dimension, Stretch, geometry::Size, style::Style};

use crate::error::Error;

pub(crate) struct TemplateItem<'a> {
    pub name:                   String,
    pub layout_node:            stretch::node::Node,
    pub texture:                sdl2::render::Texture<'a>,
    pub texture_data:           sdl2::render::TextureQuery

}

impl<'a> TemplateItem<'a> {

    pub fn default_style(width: f32, height: f32) -> Style {
        Style {
            size: Size { width: Dimension::Points(width), height: Dimension::Points(height) },
            margin: stretch::geometry::Rect { start: Dimension::Points(16.0), end: Dimension::Points(16.0), top: Dimension::Points(16.0), bottom: Dimension::Points(16.0) },
            ..Default::default()
        }
    }

    pub fn load_from_folder(folder: &Path, texture_creator: &'a TextureCreator<WindowContext>, layout: &mut Stretch) -> Result<Vec<TemplateItem<'a>>, Error> {
        println!("Loading templates from {}", folder.to_str().expect(""));
        Ok(std::fs::read_dir(folder)?.filter_map(|result| {
            let dir_entry = result.unwrap();

            println!("Loading {}...", dir_entry.path().to_str().expect(""));

            let path = dir_entry.path();
            let extension = path.extension();
            let allowed_extensions: Vec<&str> = vec!["png", "jpg", "jpeg", "webp", "bmp"];

            if extension.is_none() || !allowed_extensions.contains(extension.unwrap().to_str().as_ref().expect("")) {
                println!("  - not an image file...");
                return None;
            }

            let texture = texture_creator.load_texture(&path).unwrap();
            let texture_data = texture.query();

            println!("  - loaded {}, {}x{}", path.to_str().expect(""), texture_data.width, texture_data.height);

            Some(TemplateItem {
                name: String::from(dir_entry.file_name().to_str().expect("Could not convert filename to UTF-8")),
                layout_node: layout.new_node(Self::default_style(texture_data.width as f32, texture_data.height as f32), vec![]).unwrap(),
                texture,
                texture_data,
            })
        }).collect())
    }

}

pub(crate) struct Resources<'font, 'texture> {
    pub font_default:   Font<'font, 'static>,
    pub font_small:     Font<'font, 'static>,

    pub template_list:  Vec<TemplateItem<'texture>>,

}
