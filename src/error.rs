use image::ImageError;
use sdl2::{IntegerOrSdlError, render::{TargetRenderError, TextureValueError}, ttf::{FontError, InitError}, video::WindowBuildError};

pub(crate) enum Error {
    InitError { message: String },
    Message { message: String },
    Clipboard { message: String }
}

impl std::fmt::Debug for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Error::InitError { message } |
            Error::Message { message } => f.write_fmt(format_args!("Error: {}", message)),
            Error::Clipboard { message } => f.write_fmt(format_args!("Clipboard error: {}", message))
        }
    }
}

impl From<String> for Error {
    fn from(message: String) -> Self {
        Error::Message { message }
    }
}

impl From<InitError> for Error {
    fn from(error: InitError) -> Self {
        Error::InitError { message: error.to_string()  }
    }
}

impl From<WindowBuildError> for Error {
    fn from(_: WindowBuildError) -> Self {
        Error::InitError { message: String::from("Error: Could not build SDL2 window.") }
    }
}

impl From<IntegerOrSdlError> for Error {
    fn from(_: IntegerOrSdlError) -> Self {
        Error::Message { message: String::from("Error: IntegerOrSdlError")  }
    }
}

impl From<TextureValueError> for Error {
    fn from(error: TextureValueError) -> Self {
        Error::Message { message: String::from(format!("Error: {}", error)) }
    }
}

impl From<FontError> for Error {
    fn from(error: FontError) -> Self {
        Error::Message { message: String::from(format!("Font Error: {}", error)) }
    }
}

impl From<std::io::Error> for Error {
    fn from(error: std::io::Error) -> Self {
        Error::Message { message: String::from(format!("IO Error: {}", error)) }
    }
}

impl From<stretch::Error> for Error {
    fn from(error: stretch::Error) -> Self {
        Error::Message { message: String::from(format!("Stretch Error: {}", error)) }
    }
}

impl From<TargetRenderError> for Error {
    fn from(_: TargetRenderError) -> Self {
        Error::Message { message: String::from("Error: TargetRenderError") }
    }
}

impl From<ImageError> for Error {
    fn from(error: ImageError) -> Self {
        Error::Message { message: String::from(format!("Error: {}", error)) }
    }
}
