
use std::{rc::Rc, cell::RefCell};

use sdl2::{EventPump, keyboard::KeyboardState, Sdl, VideoSubsystem, mouse::MouseState, render::TextureCreator, video::WindowContext};
use stretch::{node::Node, Stretch};

use crate::{DoesProcess, EventResult, animation::FloatAnimation, data::*, editor::Editor, error::Error, ui::HotspotProcessor, render_queue::RenderQueue, resources::Resources, selector::Selector, utils::position_size_overlaps};

pub(crate) struct AppState<'reference, 'texture, 'font, 'queue> {
    pub sdl: RefCell<Sdl>,
    pub render_queue: RefCell<RenderQueue<'queue>>,
    pub texture_creator: &'texture TextureCreator<WindowContext>,
    pub resources: &'reference Resources<'font, 'texture>,
    pub video: RefCell<VideoSubsystem>,

    pub mouse_state: Option<MouseState>,

    pub layout: RefCell<Stretch>,
    pub root_node: Node,

    pub window_drag_offset: Vector2<i32>,
    pub resize_handle: Option<ResizeHandle>,

    pub hotspot_processor: Rc<RefCell<HotspotProcessor<'queue>>>,

    pub selector: RefCell<Selector<'texture, 'reference>>,
    pub editor: RefCell<Editor<'texture>>,

}


