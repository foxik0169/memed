use std::{ops::Add, rc::Rc, ops::Sub};

use sdl2::{pixels::PixelFormatEnum, rect::Point, rect::Rect, render::BlendMode, pixels::Color, render::{Texture, TextureCreator}, surface::Surface, video::WindowContext};
use stretch::node::Node;


use crate::{animation::ColorAnimation, error::Error};

#[derive(Copy, Clone)]
pub(crate) struct Vector2<T> {
    pub x: T,
    pub y: T
}

impl<T> Vector2<T> where T: std::default::Default {
    pub fn new(x: T, y: T) -> Self {
        Vector2 { x, y }
    }

    pub fn zero() -> Self {
        Vector2 { x: Default::default(), y: Default::default() }
    }
}

// impl Vector2<i32> {
//     pub fn new(x: i32, y: i32) -> Self {
//         Vector2 { x, y }
//     }

//     pub fn zero() -> Self {
//         Vector2 { x: 0, y: 0 }
//     }

// }

// impl Vector2<u32> {
//     pub fn new(x: u32, y: u32) -> Self {
//         Vector2 { x, y }
//     }

//     pub fn zero() -> Self {
//         Vector2 { x: 0, y:0 }
//     }

// }

impl<T> Add<Vector2<T>> for Vector2<T> where T: std::ops::Add<Output = T> {
    type Output = Vector2<T>;

    fn add(self, rhs: Vector2<T>) -> Self::Output {
        Vector2 {
            x: self.x + rhs.x,
            y: self.y + rhs.y
        }
    }
}

impl<T> Sub<Vector2<T>> for Vector2<T> where T: std::ops::Sub<Output = T> {
    type Output = Vector2<T>;

    fn sub(self, rhs: Vector2<T>) -> Self::Output {
        Vector2 {
            x: self.x - rhs.x,
            y: self.y - rhs.y
        }
    }
}

impl From<Vector2<i32>> for Point {
    fn from(other: Vector2<i32>) -> Self {
        Point::new(other.x, other.y)
    }
}

// @todo Extract to separate file (UI?)

pub(crate) struct ScrollPanel<'texture> {
    pub rect_pos:                   Vector2<i32>,
    pub smooth_target_y:            i32,
    pub texture:                    Texture<'texture>,

}

impl<'a> ScrollPanel<'a> {

    pub(crate) fn new(texture_creator: &'a TextureCreator<WindowContext>, width: u32, height: u32) -> Result<Self, Error> {
        let mut texture = texture_creator.create_texture_target(PixelFormatEnum::RGBA32, width, height)?;

        Self::apply_texture_blend(&mut texture);

        Ok(ScrollPanel {
            rect_pos: Vector2::zero(),
            smooth_target_y: 0,
            texture,
        })
    }

    pub(crate) fn resize(&mut self, texture_creator: &'a TextureCreator<WindowContext>, width: u32, height: u32) -> Result<(), Error> {
        self.texture = texture_creator.create_texture_target(PixelFormatEnum::RGBA32, width, height)?;

        Self::apply_texture_blend(&mut self.texture);

        Ok(())
    }

    fn apply_texture_blend(texture: &mut Texture) {
        texture.set_blend_mode(BlendMode::Blend);
        texture.set_alpha_mod(255);
        texture.set_color_mod(255, 255, 255);
    }

}

pub(crate) struct TextBlock<'a> {
    pub(crate) text: String,
    pub(crate) position: Vector2<i32>,
    pub(crate) size: Vector2<u32>,
    pub(crate) color: Color,

    pub(crate) surface: Option<Surface<'a>>,

}

impl TextBlock<'_> {

    pub fn set_color(&mut self, color: Color) {
        self.color = color;
        self.invalidate();
    }

    pub fn invalidate(&mut self) {
        self.surface = None;
    }

}

pub(crate) struct ColorAnimatedNode {
    pub node: Node,
    pub animation: ColorAnimation
}

#[derive(Copy, Clone)]
pub(crate) enum ResizeHandle {
    TopLeft(Vector2<i32>),
    TopRight(Vector2<i32>),
    BottomRight(Vector2<i32>),
    BottomLeft(Vector2<i32>)
}
