use lerp::Lerp;
use num::clamp;
use sdl2::pixels::Color;

pub(crate) struct FloatAnimation {
    pub time: f32,
    pub length: f32,
    pub source: f32,
    pub target: f32
}

pub(crate) struct ColorAnimation {
    pub time: f32,
    pub length: f32,
    pub source: Color,
    pub target: Color
}

impl ColorAnimation {

    pub fn new(length: f32, source: Color, target: Color) -> Self {
        ColorAnimation {
            length,
            source,
            target,
            time: 0.0
        }
    }

    pub fn is_finished(&self) -> bool {
        self.time >= self.length
    }

}

impl FloatAnimation {

    pub fn process(&mut self, delta_time: f32) -> f32 {
        self.time = self.time + delta_time;

        clamp(self.source.lerp(self.target, self.time/self.length), self.source, self.target)
    }

}

impl ColorAnimation {

    pub fn process(&mut self, delta_time: f32) -> Color {
        self.time = self.time + delta_time;

        let alpha = self.time / self.length;

        let r_source = self.source.r as f32 / 255.0;
        let g_source = self.source.g as f32 / 255.0;
        let b_source = self.source.b as f32 / 255.0;
        let a_source = self.source.a as f32 / 255.0;

        let r_target = self.target.r as f32 / 255.0;
        let g_target = self.target.g as f32 / 255.0;
        let b_target = self.target.b as f32 / 255.0;
        let a_target = self.target.a as f32 / 255.0;

        let r_out = (r_source.lerp(r_target, alpha) * 255.0) as u8;
        let g_out = (g_source.lerp(g_target, alpha) * 255.0) as u8;
        let b_out = (b_source.lerp(b_target, alpha) * 255.0) as u8;
        let a_out = (a_source.lerp(a_target, alpha) * 255.0) as u8;

        Color::RGBA(r_out, g_out, b_out, a_out)
    }

}
