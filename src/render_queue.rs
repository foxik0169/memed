use std::{collections::HashMap, cell::RefMut};
use sdl2::{render::Canvas, video::Window};

use crate::{app_state::AppState, error::Error};

type RenderCallback<'a> = Box<dyn FnMut(&mut Canvas<Window>, &AppState<'_,'_,'_,'a>) + 'a>;

pub(crate) struct RenderQueue<'a> {
    queue: HashMap<i32, Vec<RenderCallback<'a>>>,

}

impl<'a> RenderQueue<'a> {

    pub fn new() -> Self {
        RenderQueue {
            queue: HashMap::new()
        }
    }

    pub fn enqueue(&mut self, order: i32, callback: RenderCallback<'a>) {
        self.queue.entry(order)
            .or_default()
            .push(callback);
    }

    pub fn render(&mut self, canvas: &mut Canvas<Window>, app_state: &AppState<'_,'_,'_,'a>) -> Result<(), Error> {
        let mut keys: Vec<i32> = self.queue.keys().copied().collect();
        keys.sort_unstable();

        for key in keys.iter() {
            let mut entries: Vec<&mut RenderCallback> = self.queue.entry(*key).or_default().into_iter().collect();

            for callback in entries.iter_mut() {
                callback(canvas, app_state);
            }
        }

        self.queue.clear();

        Ok(())
    }

}
