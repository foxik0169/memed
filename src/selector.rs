use std::{cell::RefCell, collections::HashMap, rc::{Rc, Weak}};

use sdl2::{event::Event, EventPump, mouse::MouseButton, mouse::MouseWheelDirection, pixels::{Color, PixelFormatEnum}, rect::Rect, render::{BlendMode, Canvas, TextureCreator}, video::Window, video::{WindowContext, WindowPos}};
use stretch::{node::Node, Stretch};
use lerp::Lerp;

use crate::{DoesProcess, EventResult, animation::ColorAnimation, animation::FloatAnimation, app_state::AppState, resources::TemplateItem, data::{ColorAnimatedNode, ScrollPanel}, data::Vector2, error::Error, resources::Resources, ui::HotspotHandle, utils::is_item_overlaping_position};

pub(crate) struct Selector<'texture, 'reference> {
    pub hotspot_layout: HashMap<HotspotHandle, Node>,
    pub hotspot_template: HashMap<HotspotHandle, &'reference TemplateItem<'texture>>,

    pub scroll_panel: ScrollPanel<'texture>,
    pub scroll_panel_dirty: bool,

}

impl<'texture, 'reference> Selector<'texture, 'reference> {

    pub fn initialize(&mut self, app_state: &AppState<'reference,'texture,'_,'_>) {
        let mut hotspot_processor = app_state.hotspot_processor.borrow_mut();

        const HOTSPOT_HOVER_COLOR: Color = Color::RGBA(115,20,255,170);

        for (index, reference) in app_state.resources.template_list.iter().enumerate() {
            let hotspot_handle: HotspotHandle = hotspot_processor.create_hotspot(HOTSPOT_HOVER_COLOR, -1);

            self.hotspot_template.insert(hotspot_handle, reference);
            self.hotspot_layout.insert(hotspot_handle, reference.layout_node);

            let hotspot = hotspot_processor.get_hotspot_mut(hotspot_handle).unwrap();

            // Bind the hotspots

            hotspot.callback_click = Some(Box::new(move |app_state: &AppState| {
                let mut editor = app_state.editor.borrow_mut();
                editor.selected_template_index = Some(index);
                editor.texture_dirty = true;
            }));

            hotspot.callback_hover_enter = Some(Box::new(move |app_state: &AppState| {
                println!("Hotspot hover enter!");
            }));

            hotspot.callback_hover_leave = Some(Box::new(move |app_state: &AppState| {
                println!("Hotspot hover exit!");
            }));

        }
    }

    pub fn event(event: &Event, canvas: &mut Canvas<Window>, app_state: &'texture AppState) -> Result<EventResult, Error> {
        let mut selector = app_state.selector.borrow_mut();
        let texture_creator = app_state.texture_creator;

        match event {
            Event::MouseWheel { direction, x, y, .. } => {
                let (_canvas_width, cavnas_height) = canvas.output_size()?;
                let scroll_panel = &mut selector.scroll_panel;

                match direction {
                    MouseWheelDirection::Normal => scroll_panel.smooth_target_y = num::clamp(scroll_panel.smooth_target_y - y * 150, 0, scroll_panel.texture.query().height as i32 - cavnas_height as i32),
                    _ => {}
                }
            },

            _ => {}
        }
        Ok(EventResult::Continue)
    }

}

impl<'queue, 'texture> DoesProcess<'texture, 'queue> for Selector<'_, '_> {
    fn process(&mut self, canvas: &mut Canvas<Window>, app_state: &AppState<'_,'texture,'_,'queue>) -> Result<(), Error> {
        let editor = app_state.editor.borrow();
        let layout = app_state.layout.borrow();

        // Update hotspot layout

        const BORDER_WIDTH: u32 = 8;
        const ORDER_HOVER_EFFECTS: i32 = ORDER_SCROLL_TEXTURE-1;

        for (hotspot_handle, node) in self.hotspot_layout.iter() {
            let mut hotspot_processor = app_state.hotspot_processor.borrow_mut();
            let layout = app_state.layout.borrow();

            let node_layout = layout.layout(*node)?;

            let hotspot = hotspot_processor.get_hotspot_mut(*hotspot_handle);
            if hotspot.is_some() {
                let mut hotspot = hotspot.unwrap();

                hotspot.position = Vector2::new(node_layout.location.x as i32 - self.scroll_panel.rect_pos.x - BORDER_WIDTH as i32, node_layout.location.y as i32 - self.scroll_panel.rect_pos.y - BORDER_WIDTH as i32);
                hotspot.size     = Vector2::new((node_layout.size.width as u32) + BORDER_WIDTH*2, (node_layout.size.height as u32) + BORDER_WIDTH*2);
            }
        }

        // Update smooth scrolling behaviour

        let smooth_scroll = (self.scroll_panel.rect_pos.y as f32).lerp(self.scroll_panel.smooth_target_y as f32, 0.25 /* @todo delta time */);

        self.scroll_panel.rect_pos.y = smooth_scroll as i32;

        // Render images to scroll texture

        if self.scroll_panel_dirty {
            canvas.with_texture_canvas(&mut self.scroll_panel.texture, |canvas| {
                canvas.set_draw_color(Color::RGBA(0,0,0,0));
                canvas.clear();

                for template in &app_state.resources.template_list {
                    let current_layout = layout.layout(template.layout_node).unwrap();

                    canvas.copy(&template.texture, None, Rect::new(current_layout.location.x as i32, current_layout.location.y as i32, current_layout.size.width as u32, current_layout.size.height as u32)).unwrap();
                }
            })?;

            self.scroll_panel_dirty = false;
        }

        let (canvas_width, canvas_height) = canvas.output_size()?;
        let scroll_rect_pos = self.scroll_panel.rect_pos;

        // Render scroll texture to viewport

        const ORDER_SCROLL_TEXTURE: i32 = 0;

        let mut render_queue = app_state.render_queue.borrow_mut();

        render_queue.enqueue(ORDER_SCROLL_TEXTURE, Box::new(move |canvas, app_state| {
            let selector = app_state.selector.borrow();

            canvas.set_blend_mode(BlendMode::Blend);
            canvas.copy(&selector.scroll_panel.texture, Rect::new(scroll_rect_pos.x, scroll_rect_pos.y, canvas_width, canvas_height), None).unwrap();
        }));

        Ok(())
    }
}
