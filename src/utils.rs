use crate::resources::TemplateItem;
use crate::data::Vector2;

pub(crate) fn layout_overlaps(layout: &stretch::result::Layout, x: f32, y: f32) -> bool
{
    layout.location.x <= x && layout.location.y <= y && layout.location.x + layout.size.width >= x && layout.location.y + layout.size.height >= y
}

pub(crate) fn position_size_overlaps(position: Vector2<i32>, size: Vector2<u32>, point_position: Vector2<i32>) -> bool {
    point_position.x >= position.x && point_position.y >= position.y && point_position.x <= position.x + size.x as i32 && point_position.y <= position.y + size.y as i32
}

pub(crate) fn position_size_overlaps_edge_inside(position: Vector2<i32>, size: Vector2<u32>, point_position: Vector2<i32>, edge: u32) -> bool {
    position_size_overlaps(position, size, point_position) && !position_size_overlaps(position + Vector2::new(edge as i32, edge as i32), size - Vector2::new(edge, edge), point_position)
}

pub(crate) fn is_near(a: Vector2<i32>, b: Vector2<i32>, threshold: i32) -> bool {
    a.x >= b.x - threshold && a.x <= b.x + threshold && a.y >= b.y - threshold && a.y <= b.y + threshold
}

pub(crate) fn is_item_overlaping_position<'a, 'b>(layout: &'a stretch::Stretch, list: &'b Vec<TemplateItem<'b>>, x: f32, y: f32) -> Option<usize> {
    list.iter().position(|item| {
        let node_layout = layout.layout(item.layout_node).unwrap();
        layout_overlaps(node_layout, x, y)
    })
}
