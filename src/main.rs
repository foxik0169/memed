// #![windows_subsystem = "windows"]
extern crate sdl2;
extern crate image;

mod error;
mod data;
mod utils;

mod app_state;
mod resources;

mod render_queue;

mod selector;
mod editor;

mod animation;
mod ui;

use std::{collections::HashMap, cell::RefCell, path::Path, rc::{Rc, Weak}};

use sdl2::{EventPump, event::Event, event::WindowEvent, keyboard::Keycode, mouse::MouseButton, mouse::MouseUtil, pixels::Color, mouse::MouseState, pixels::PixelFormatEnum, rect::Rect, render::{Canvas, TextureCreator}, video::WindowPos, video::{Window, WindowContext}};

use error::Error;
use resources::{Resources, TemplateItem};
use app_state::AppState;
use data::*;
use editor::Editor;
use selector::Selector;
use stretch::{geometry::Size, Stretch, style::{AlignItems, Dimension, FlexWrap, JustifyContent, Style}};
use ui::HotspotProcessor;
use utils::is_near;
use render_queue::RenderQueue;

enum EventResult {
    Handled,
    Continue,
    Exit
}

pub(crate) trait DoesEvent {
    fn event<'a>(&mut self, event: Event, canvas: &mut Canvas<Window>, app_state: &AppState<'_, '_, '_, '_>) -> Result<EventResult, Error>;
}

pub(crate) trait DoesProcess<'texture, 'queue> {
    fn process(&mut self, canvas: &mut Canvas<Window>, app_state: &AppState<'_, 'texture, '_, 'queue>) -> Result<(), Error>;
}

fn window_resized<'a>(app_state: &'a mut AppState<'_, '_, '_, '_>, width: i32, height: i32) -> Result<(), Error> {
    let mut layout = app_state.layout.borrow_mut();
    let root_node = app_state.root_node;

    layout.compute_layout(root_node, stretch::geometry::Size { width: stretch::number::Number::Defined(width as f32), height: stretch::number::Number::Undefined })?;

    let root_node_layout = layout.layout(root_node)?;
    let mut selector = app_state.selector.borrow_mut();

    selector.scroll_panel_dirty = true;
    selector.scroll_panel.resize(app_state.texture_creator, width as u32, root_node_layout.size.height as u32)
}

fn process_event<'a, 'texture, 'font, 'queue>(event: Event, canvas: &mut Canvas<Window>, app_state: &'a mut AppState<'_,'_,'_,'_>) -> Result<EventResult, Error>  {
    use EventResult::*;

    match event {
        Event::MouseButtonDown { mouse_btn: MouseButton::Left, x, y, .. } => {
            let window_position = canvas.window().position();
            let window_position = Vector2::new(window_position.0, window_position.1);

            let window_size = canvas.window().size();
            let window_size = Vector2::new(window_size.0 as i32, window_size.1 as i32);

            let mut cursor_position = Vector2::new(0, 0);

            unsafe {
                sdl2::sys::SDL_GetGlobalMouseState(&mut cursor_position.x, &mut cursor_position.y);
            }

            const RESIZE_HANDLE_NEAR: i32 = 16;

            if is_near(cursor_position, window_position, RESIZE_HANDLE_NEAR) {
                app_state.resize_handle = Some(ResizeHandle::TopLeft(cursor_position - window_position))
            } else if is_near(cursor_position, window_position + Vector2::new(window_size.x, 0), RESIZE_HANDLE_NEAR) {
                app_state.resize_handle = Some(ResizeHandle::TopRight(cursor_position - window_position))
            } else if is_near(cursor_position, window_position + window_size, RESIZE_HANDLE_NEAR) {
                app_state.resize_handle = Some(ResizeHandle::BottomRight(cursor_position - window_position))
            } else if is_near(cursor_position, window_position + Vector2::new(0, window_size.y), RESIZE_HANDLE_NEAR) {
                app_state.resize_handle = Some(ResizeHandle::BottomRight(cursor_position - window_position))
            }

            if app_state.resize_handle.is_some() {
                app_state.sdl.borrow().mouse().capture(true);
            }
        },

        Event::MouseButtonDown { mouse_btn: MouseButton::Right, x, y,.. } => {
            app_state.window_drag_offset.x = x;
            app_state.window_drag_offset.y = y
        },

        Event::MouseButtonUp { mouse_btn: MouseButton::Left, .. } => {
            if app_state.resize_handle.is_some() {
                app_state.resize_handle = None;
                app_state.sdl.borrow().mouse().capture(false);
            }
        },

        Event::MouseMotion { mousestate, x, y, .. } => {
            let mut cursor_position = Vector2::new(0, 0);
            unsafe {
                sdl2::sys::SDL_GetGlobalMouseState(&mut cursor_position.x, &mut cursor_position.y);
            }

            if mousestate.is_mouse_button_pressed(MouseButton::Right) {
                let window = canvas.window_mut();

                window.set_position(WindowPos::Positioned(cursor_position.x - app_state.window_drag_offset.x), WindowPos::Positioned(cursor_position.y - app_state.window_drag_offset.y))
            }

            if app_state.resize_handle.is_some() {
                let mut window = canvas.window_mut();

                let window_position = window.position();
                let window_position = Vector2::new(window_position.0, window_position.1);

                let window_size = window.size();
                let window_size = Vector2::new(window_size.0 as i32, window_size.1 as i32);

                match app_state.resize_handle.as_ref().unwrap() {
                    ResizeHandle::TopLeft(origin) => {
                        let top_right_x = window_position.x + window_size.x;
                        let bottom_left_y = window_position.y + window_size.y;

                        cursor_position = cursor_position;

                        window.set_position(WindowPos::Positioned(cursor_position.x), WindowPos::Positioned(cursor_position.y));

                        window.set_size((top_right_x - cursor_position.x) as u32, (bottom_left_y - cursor_position.y) as u32)?;

                        // @todo this code regenerates editor texture after resize, should be moved to invalidation, similarly as TextBlock

                        let mut editor = app_state.editor.borrow_mut();
                        let texture_creator = app_state.texture_creator;

                        // @todo defer to the end of event_pump's poll iter cycle

                        if editor.selected_template_index.is_some() {
                            let template = &app_state.resources.template_list[editor.selected_template_index.unwrap()];

                            editor.texture = texture_creator.create_texture_target(PixelFormatEnum::RGBA32, template.texture_data.width, template.texture_data.height)?;

                            canvas.with_texture_canvas(&mut editor.texture, |canvas| {
                                canvas.copy(&template.texture, None, None).unwrap()
                            })?
                        }

                    },
                    ResizeHandle::TopRight(origin) => {

                    },
                    ResizeHandle::BottomRight(origin) => {

                    },
                    ResizeHandle::BottomLeft(origin) => {

                    },

                    _ => {}
                }

                let window_size = canvas.window().size();
                let window_size = Vector2::new(window_size.0 as i32, window_size.1 as i32);

                window_resized(app_state, window_size.x, window_size.y)?;
            }
        },

        Event::Window { win_event: WindowEvent::Resized(width, height), .. } => {
            window_resized(app_state, width, height)?;
        },
        Event::Quit { .. } |
        Event::KeyDown { keycode: Some(Keycode::Escape), .. } => {
            let mut editor = app_state.editor.borrow_mut();
            let should_exit_editor = editor.selected_template_index.is_some();

            if should_exit_editor {
                editor.selected_template_index = None;
                return Ok(EventResult::Continue);
            }

            return Ok(Exit)
        },
        _ => {}
    };

    let mut editor = app_state.editor.borrow_mut();
    let result = editor.event(event.clone(), canvas, app_state)?;

    match result {
        EventResult::Continue => {},
        _ => return Ok(result)
    }

    let result = Selector::event(&event, canvas, app_state)?;

    match Selector::event(&event, canvas, app_state)? {
        _ => return Ok(result)
    }
}

fn main() -> Result<(), Error> {
    let width = 1280;
    let height = 720;

    let font_default_path = Path::new("./resource/default_font.ttf");

    let mut sdl = sdl2::init()?;
    let ttf = sdl2::ttf::init()?;

    let video = sdl.video()?;
    let window = video
            .window("meme'D", width, height)
            .borderless()
            .resizable()
            .build()?;

    let mut event_pump = sdl.event_pump()?;
    let mut canvas = window.into_canvas().present_vsync().accelerated().build()?;

    let texture_creator = canvas.texture_creator();

    let mut layout = stretch::node::Stretch::new();

    let resources = Resources {
        font_default: ttf.load_font(font_default_path, 32)?,
        font_small:   ttf.load_font(font_default_path, 16)?,

        template_list: TemplateItem::load_from_folder(Path::new("./templates"), &texture_creator, &mut layout)?
    };

    const COLOR_ACCENT: Color = Color::RGBA(115,20,255,200);

    let root = layout.new_node(Style {
        size: Size { width: Dimension::Percent(1.0), height: Dimension::Percent(1.0) },
        flex_wrap: FlexWrap::Wrap,
        padding: stretch::geometry::Rect { start: Dimension::Points(16.0), end: Dimension::Points(16.0), top: Dimension::Points(16.0), bottom: Dimension::Points(16.0) },
        justify_content: JustifyContent::Center,
        align_items: AlignItems::Center,
        ..Default::default()
    }, resources.template_list.iter().map(|item| item.layout_node).collect())?;

    layout.compute_layout(root, Size { width: stretch::number::Number::Defined(width as f32), height: stretch::number::Number::Undefined })?;

    let app_label = resources.font_default.render("meme'D").blended(COLOR_ACCENT)?;
    let app_label = &app_label.as_texture(&texture_creator)?;

    let texture_creator = canvas.texture_creator();

    let selector = {
        RefCell::new(Selector {
            hotspot_layout: HashMap::new(),
            hotspot_template: HashMap::new(),

            scroll_panel: ScrollPanel::new(&texture_creator, width, height)?,
            scroll_panel_dirty: true
        })
    };

    let editor = {
        RefCell::new(Editor {
            selected_template_index: None,
            texture: texture_creator.create_texture_target(PixelFormatEnum::RGBA32, 32, 32)?,
            texture_dirty: false,
            text_blocks: vec![],
            hovered_text_block: None,

            selected_text_block: None,
            selected_text_block_drag_offset: Vector2::zero(),
            selected_text_block_move: false,
            selected_text_block_resize_handle: None,

            swatches: vec![Color::BLACK, Color::WHITE, Color::RED, Color::GREEN, Color::BLUE, COLOR_ACCENT],
            hovered_swatch: None
        })
    };

    let app_state = Rc::new(RefCell::new(AppState {
        sdl: RefCell::new(sdl),
        render_queue: RefCell::new(RenderQueue::new()),
        texture_creator: &texture_creator,
        resources: &resources,
        video: RefCell::new(video),

        mouse_state: None,

        layout: RefCell::new(layout),
        root_node: root,

        window_drag_offset: Vector2::zero(),
        resize_handle: None,

        hotspot_processor: Rc::new(RefCell::new(HotspotProcessor::new())),

        selector,
        editor
    }));

    let hotspot_processor = Rc::downgrade(&app_state.borrow().hotspot_processor);

    let processors: Vec<Weak<RefCell<dyn DoesProcess>>> = vec![
        hotspot_processor.clone()
    ];

    let event_handlers: Vec<Weak<RefCell<dyn DoesEvent>>> = vec![
        hotspot_processor.clone()
    ];

    {
        let app_state = app_state.borrow();
        let mut selector = app_state.selector.borrow_mut();
        let mut editor = app_state.editor.borrow_mut();

        selector.initialize(&app_state);
        editor.initialize(&app_state);
    }

    // @todo move to initialization of selector
    app_state.borrow().selector.borrow_mut().scroll_panel.resize(app_state.borrow().texture_creator, width, app_state.borrow().layout.borrow_mut().layout(root)?.size.height as u32)?;

    'main_loop: loop {

        {
            let event_buffer: Vec<Event> = event_pump.poll_iter().collect();

            let mut app_state = app_state.borrow_mut();
            app_state.mouse_state = Some(event_pump.mouse_state());

            'event: for event in event_buffer.into_iter() {
                match process_event(event.clone(), &mut canvas, &mut app_state)? {
                    EventResult::Continue => (),                 // apply processor events
                    EventResult::Handled  => continue 'event,    // event consumed
                    EventResult::Exit     => break 'main_loop    // exit
                }

                for processor in event_handlers.iter() {
                    let processor = processor.upgrade();

                    if processor.is_some() {
                        let processor = processor.unwrap();
                        let mut processor = processor.borrow_mut();

                        // @todo respond to the result
                        processor.event(event.clone(), &mut canvas, &mut app_state)?;
                    }
                }
            }
        }

        canvas.set_draw_color(Color::RGB(18, 18, 18));
        canvas.clear();

        {

            {
                let app_state = app_state.borrow();

                {
                    let mut selector = app_state.selector.borrow_mut();
                    selector.process(&mut canvas, &app_state)?;

                    let mut editor = app_state.editor.borrow_mut();
                    editor.process(&mut canvas, &app_state)?;

                }

                for processor in processors.iter() {
                    let processor = processor.upgrade();

                    if processor.is_some() {
                        let processor = processor.unwrap();
                        let mut processor = processor.borrow_mut();

                        processor.process(&mut canvas, &app_state)?;
                    }
                }
            }

            let app_state = app_state.borrow();

            let mut render_queue = app_state.render_queue.borrow_mut();
            render_queue.render(&mut canvas, &app_state)?;
        }

        canvas.copy(app_label, None, Rect::new(18, 0, app_label.query().width, app_label.query().height))?;

        let (width, height) = canvas.output_size()?;

        canvas.set_draw_color(Color::RGBA(115,20,255,100));
        canvas.draw_rect(Rect::new(0,0, width, height))?;

        canvas.present();
    }

    Ok(())
}
