use sdl2::{EventPump, event::Event, keyboard::{Keycode, Mod}, mouse::MouseButton, pixels::{Color, PixelFormatEnum}, rect::Rect, render::Canvas, render::{Texture, TextureCreator}, surface::Surface, video::Window, video::WindowContext};
use stretch::{node::Node, Stretch};

use crate::{DoesProcess, EventResult, app_state::AppState, DoesEvent, data::{TextBlock, Vector2}, data::ResizeHandle, error::Error, render_queue::RenderQueue, resources::{Resources, TemplateItem}, utils::{is_near, position_size_overlaps}};

use image::{GenericImage, GenericImageView, ImageBuffer, ImageEncoder, RgbImage, RgbaImage};

pub(crate) struct Editor<'texture> {
    pub selected_template_index: Option<usize>,

    pub texture: Texture<'texture>,
    pub texture_dirty: bool,

    pub text_blocks: Vec<TextBlock<'texture>>,
    pub hovered_text_block: Option<usize>,

    pub selected_text_block: Option<usize>,
    pub selected_text_block_drag_offset: Vector2<i32>,
    pub selected_text_block_move: bool,
    pub selected_text_block_resize_handle: Option<ResizeHandle>,

    pub swatches: Vec<Color>,
    pub hovered_swatch: Option<usize>

}

impl<'texture> Editor<'texture> {

    pub fn initialize(&mut self, app_state: &AppState) {

        // Spawn hotspots for colors!

        let hotspot_processor = app_state.hotspot_processor.borrow_mut();

        

    }

    fn refresh_editor_texture<'a>(&'a mut self, template: &'a TemplateItem<'texture>, texture_creator: &'texture TextureCreator<WindowContext>, canvas: &'a mut Canvas<Window>) {
        if self.selected_template_index.is_some() {
            self.texture = texture_creator.create_texture_target(PixelFormatEnum::RGBA32, template.texture_data.width, template.texture_data.height).unwrap();

            canvas.with_texture_canvas(&mut self.texture, |canvas| {
                canvas.copy(&template.texture, None, None).unwrap()
            }).unwrap()
        }
    }

    fn calculate_texture_rect(&self, canvas: &mut Canvas<Window>) -> Result<Rect, Error> {
        let (canvas_width, canvas_height) = canvas.output_size()?;
        let texture_query = self.texture.query();

        Ok(Rect::new(((canvas_width as f32/2.0) - (texture_query.width as f32/2.0)) as i32, ((canvas_height as f32/2.0) - (texture_query.height as f32/2.0)) as i32, texture_query.width, texture_query.height))
    }

}

impl<'texture, 'queue> DoesEvent for Editor<'texture> {
    fn event(&mut self, event: Event, canvas: &mut Canvas<Window>, app_state: &AppState) -> Result<EventResult, Error> {
        if self.selected_template_index.is_none() {
            return Ok(EventResult::Continue);
        }

        let texture_creator = app_state.texture_creator;

        match event {

            // @note Chunk out the handling to sub functions maybe?

            Event::KeyDown { keycode: Some(Keycode::C), keymod, .. } => {

                if !keymod.contains(Mod::LCTRLMOD) {
                    return Ok(EventResult::Continue);
                }

                // Copy current image to clipboard as bitmap image.

                let texture_query = self.texture.query();

                let mut output_texture = texture_creator.create_texture_target(PixelFormatEnum::RGB24, texture_query.width, texture_query.height)?;

                let mut output_data: Vec<u8> = vec![];

                canvas.with_texture_canvas(&mut output_texture, |canvas| {
                    canvas.copy(&self.texture, None, None).unwrap();

                    for text_block in self.text_blocks.iter() {
                        let text_block_surface = text_block.surface.as_ref().expect("Tried to read surface that was not rendered.");

                        let texture_creator = canvas.texture_creator();

                        let text_block_texture = text_block_surface.as_texture(&texture_creator).unwrap();

                        canvas.copy(&text_block_texture, None, Rect::new(text_block.position.x, text_block.position.y, text_block_surface.width(), text_block_surface.height().min(text_block.size.y))).unwrap();
                    }

                    output_data = canvas.read_pixels(None, PixelFormatEnum::RGB24).unwrap();
                })?;

                let mut bmp_buffer: Vec<u8> = vec![];
                let encoder = image::bmp::BmpEncoder::new(&mut bmp_buffer);

                encoder.write_image(&output_data[..], texture_query.width, texture_query.height, image::ColorType::Rgb8)?;

                clipboard_win::set_clipboard(clipboard_win::formats::Bitmap, &bmp_buffer[..]).map_err(|err| { crate::error::Error::Clipboard { message: format!("{}", err) } })?;
            },
            Event::MouseButtonUp { mouse_btn: MouseButton::Left, .. } => {
                self.selected_text_block_move = false;
                self.selected_text_block_resize_handle = None;
            },
            Event::MouseButtonDown { mouse_btn: MouseButton::Left, x, y, .. } => {
                if self.hovered_swatch.is_some() {
                    if self.selected_text_block.is_some() {
                        let swatch_index = self.hovered_swatch.unwrap();
                        let swatch = self.swatches[swatch_index];

                        let text_block_index = self.selected_text_block.unwrap();
                        let text_block = &mut self.text_blocks[text_block_index];

                        text_block.set_color(swatch);
                    }

                    return Ok(EventResult::Handled);
                }

                if self.selected_template_index.is_some() && self.selected_text_block.is_some() && self.hovered_text_block.is_none() {
                    self.selected_text_block = None;
                    app_state.video.borrow_mut().text_input().stop();
                    return Ok(EventResult::Handled);
                }

                // Handle text block resize

                let self_texture_rect = self.calculate_texture_rect(canvas)?;

                if self.selected_text_block.is_some() {
                    let text_block = &self.text_blocks[self.selected_text_block.unwrap()];

                    let cursor_position = Vector2::new(x, y) - Vector2::new(self_texture_rect.x(), self_texture_rect.y());

                    const RESIZE_HANDLE_NEAR: i32 = 8;

                    let origin = cursor_position - text_block.position;

                    if is_near(cursor_position, text_block.position, RESIZE_HANDLE_NEAR) {
                        self.selected_text_block_resize_handle = Some(ResizeHandle::TopLeft(origin));
                    } else if is_near(cursor_position, text_block.position + Vector2::new(text_block.size.x as i32, 0), RESIZE_HANDLE_NEAR) {
                        self.selected_text_block_resize_handle = Some(ResizeHandle::TopRight(origin));
                    } else if is_near(cursor_position, text_block.position + Vector2::new(text_block.size.x as i32, text_block.size.y as i32), RESIZE_HANDLE_NEAR) {
                        self.selected_text_block_resize_handle = Some(ResizeHandle::BottomRight(origin))
                    } else if is_near(cursor_position, text_block.position + Vector2::new(0, text_block.size.y as i32), RESIZE_HANDLE_NEAR) {
                        self.selected_text_block_resize_handle = Some(ResizeHandle::BottomLeft(origin))
                    }

                    if self.selected_text_block_resize_handle.is_some() {
                        return Ok(EventResult::Handled)
                    }
                }

                if self.selected_template_index.is_some() && self.hovered_text_block.is_some() {
                    self.selected_text_block = self.hovered_text_block;

                    let text_block = &self.text_blocks[self.selected_text_block.unwrap()];

                    self.selected_text_block_drag_offset = Vector2::new(x, y) - text_block.position;
                    self.selected_text_block_move = true;

                    app_state.video.borrow_mut().text_input().start();

                    return Ok(EventResult::Handled);
                } else if self.selected_template_index.is_some() {
                    let new_text_block = TextBlock {
                        position: Vector2::new(x - 256/2, y - 128/2) - Vector2::new(self_texture_rect.x(), self_texture_rect.y()),
                        size: Vector2::new(256, 128),
                        text: String::from("Your incredible meme here..."),
                        color: Color::BLACK,
                        surface: None,
                    };

                    self.selected_text_block_drag_offset = Vector2::new(x, y) - new_text_block.position;

                    self.text_blocks.push(new_text_block);
                    self.selected_text_block = Some(self.text_blocks.len()-1);

                    app_state.video.borrow_mut().text_input().start();

                    return Ok(EventResult::Handled);
                }

                return Ok(EventResult::Handled);
            },
            Event::MouseWheel { .. } => { return Ok(EventResult::Handled) },
            Event::MouseMotion { mousestate, x, y, .. } => {
                if self.selected_text_block_resize_handle.is_some() {

                    // @todo Implement minimal size of the edited block & also at window.

                    let (canvas_width, canvas_height) = canvas.output_size()?;
                    let texture_query = self.texture.query();

                    let self_texture_rect = Rect::new(((canvas_width as f32/2.0) - (texture_query.width as f32/2.0)) as i32, ((canvas_height as f32/2.0) - (texture_query.height as f32/2.0)) as i32, texture_query.width, texture_query.height);

                    let resize_handle = self.selected_text_block_resize_handle.unwrap();

                    let text_block_index = self.selected_text_block.unwrap();
                    let text_block = &mut self.text_blocks[text_block_index];

                    let cursor_position = Vector2::new(x, y) - Vector2::new(self_texture_rect.x(), self_texture_rect.y());

                    match resize_handle {
                        ResizeHandle::TopLeft(origin) => {
                            let top_right_x = text_block.position.x + text_block.size.x as i32;
                            let bottom_left_y = text_block.position.y + text_block.size.y as i32;

                            text_block.position = cursor_position;
                            text_block.size = Vector2::new((top_right_x - cursor_position.x) as u32, (bottom_left_y - cursor_position.y) as u32);

                            text_block.invalidate();
                        },
                        ResizeHandle::TopRight(origin) => {
                        },
                        _ => {}
                    }

                    return Ok(EventResult::Handled);
                }

                if mousestate.is_mouse_button_pressed(MouseButton::Left) && self.selected_text_block.is_some() && self.selected_text_block_move {
                    let drag_offset = self.selected_text_block_drag_offset;

                    let text_block_index = self.selected_text_block.unwrap();
                    let selected_text_block = &mut self.text_blocks[text_block_index];

                    selected_text_block.position = Vector2::new(x, y) - drag_offset;

                    return Ok(EventResult::Handled);
                }

                return Ok(EventResult::Handled);
            },
            Event::KeyDown { keycode: Some(Keycode::Delete), .. } => {
                if self.selected_text_block.is_some() {
                    let text_block_index = self.selected_text_block.unwrap();

                    self.text_blocks.remove(text_block_index);
                    self.selected_text_block = None;
                }
            },
            Event::KeyDown { keycode: Some(Keycode::Backspace), .. } => {
                if self.selected_text_block.is_some() {
                    let text_block_index = self.selected_text_block.unwrap();
                    let text_block = &mut self.text_blocks[text_block_index];

                    text_block.text.pop();
                    text_block.invalidate();
                }
            },
            Event::TextInput { text, .. } => {
                if self.selected_text_block.is_some() {
                    let text_block_index = self.selected_text_block.unwrap();
                    let text_block = &mut self.text_blocks[text_block_index];

                    text_block.text.push_str(text.as_str());
                    text_block.invalidate();
                }
            }
            _ => {}
        }

        Ok(EventResult::Continue)
    }
}

impl<'texture, 'queue> DoesProcess<'texture, 'queue> for Editor<'texture> {
    fn process(&mut self, canvas: &mut Canvas<Window>, app_state: &AppState<'_, 'texture, '_, 'queue>) -> Result<(), Error> {
        let mut render_queue = app_state.render_queue.borrow_mut();

        if self.selected_template_index.is_none() {
            return Ok(());
        }

        if self.texture_dirty {
            self.refresh_editor_texture(&app_state.resources.template_list[self.selected_template_index.unwrap()], app_state.texture_creator, canvas);
        }

        // Clear with see-through background

        render_queue.enqueue(ORDER_EDITOR_TEXTURE-1, Box::new(move |canvas, _app_state| {
            canvas.set_draw_color(Color::RGBA(18,18,18, 200));
            canvas.fill_rect(None).unwrap();
        }));

        // Draw editor texture, contains base image...

        const ORDER_EDITOR_TEXTURE: i32 = 10;

        let editor_texture_dest_rect = self.calculate_texture_rect(canvas)?;

        render_queue.enqueue(ORDER_EDITOR_TEXTURE, Box::new(move |canvas, app_state| {
            let editor = app_state.editor.borrow();
            canvas.copy(&editor.texture, None, editor_texture_dest_rect).unwrap();
        }));

        // Update hovered textblock

        self.hovered_text_block = None;

        for (index, item) in self.text_blocks.iter().enumerate() {
            let mouse_x = app_state.mouse_state.unwrap().x();
            let mouse_y = app_state.mouse_state.unwrap().y();

            let does_overlap = position_size_overlaps(item.position, item.size, Vector2::new(mouse_x, mouse_y) - Vector2::new(editor_texture_dest_rect.x(), editor_texture_dest_rect.y()));

            if does_overlap {
                self.hovered_text_block = Some(index);
                break;
            }
        }

        // Draw editor texts

        render_queue.enqueue(ORDER_EDITOR_TEXTURE+1, Box::new(move | canvas, app_state| {
            let texture_creator = &app_state.texture_creator;
            let mut editor = app_state.editor.borrow_mut();

            for text_block in editor.text_blocks.iter_mut() {
                if text_block.text.is_empty() {
                    continue;
                }

                if text_block.surface.is_none() {
                    text_block.surface = Some(app_state.resources.font_default.render(text_block.text.as_str()).blended_wrapped(text_block.color, text_block.size.x).unwrap());
                }

                let surface = text_block.surface.as_mut().unwrap();

                let src_rect = Rect::new(0, 0, text_block.size.x.min(surface.width()), text_block.size.y.min(surface.height()));
                let dst_rect = Rect::new(text_block.position.x + editor_texture_dest_rect.x(), text_block.position.y + editor_texture_dest_rect.y(), src_rect.width(), src_rect.height());

                canvas.copy(&surface.as_texture(&texture_creator).unwrap(), src_rect, dst_rect).unwrap();
            }
        }));

        if self.hovered_text_block.is_some() {
            let text_block = &self.text_blocks[self.hovered_text_block.unwrap()];

            let text_block_position = text_block.position;
            let text_block_size = text_block.size;

            render_queue.enqueue(ORDER_EDITOR_TEXTURE+1, Box::new(move |canvas, _app_state| {
                canvas.set_draw_color(Color::BLUE);
                canvas.draw_rect(Rect::new(text_block_position.x + editor_texture_dest_rect.x(), text_block_position.y + editor_texture_dest_rect.y(), text_block_size.x, text_block_size.y)).unwrap();
            }));
        }

        if self.selected_text_block.is_some() {
            let text_block = &self.text_blocks[self.selected_text_block.unwrap()];

            let text_block_position = text_block.position;
            let text_block_size = text_block.size;

            render_queue.enqueue(ORDER_EDITOR_TEXTURE+2, Box::new(move |canvas, _app_state| {
                canvas.set_draw_color(Color::RED);
                canvas.draw_rect(Rect::new(text_block_position.x + editor_texture_dest_rect.x(), text_block_position.y + editor_texture_dest_rect.y(), text_block_size.x, text_block_size.y)).unwrap();
            }));
        }

        // Color swatches

        const SWATCHES_OFFSET: i32 = 16;
        const SWATCHES_SIZE: u32 = 32;

        self.hovered_swatch = None;

        render_queue.enqueue(ORDER_EDITOR_TEXTURE+3, Box::new(move |canvas, app_state| {
            let mut editor = app_state.editor.borrow_mut();
            let swatches = Vec::from(&editor.swatches[..]);

            for (index, swatch) in swatches.iter().enumerate() {
                canvas.set_draw_color(*swatch);

                let (x, y) = (editor_texture_dest_rect.x() + editor_texture_dest_rect.width() as i32 + SWATCHES_OFFSET, editor_texture_dest_rect.y() + ((8 + SWATCHES_SIZE as i32) * index as i32));

                canvas.fill_rect(Rect::new(x, y, SWATCHES_SIZE, SWATCHES_SIZE)).unwrap();

                let mouse_state = app_state.mouse_state.unwrap();

                if position_size_overlaps(Vector2::new(x, y), Vector2::new(SWATCHES_SIZE, SWATCHES_SIZE), Vector2::new(mouse_state.x(), mouse_state.y())) {
                    editor.hovered_swatch = Some(index);
                }
            }
        }));

        Ok(())
    }

}
