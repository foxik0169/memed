use std::{ops::Fn, collections::HashMap};
use sdl2::{event::Event, pixels::Color, mouse::MouseButton, rect::Rect, render::Canvas, video::Window};

use crate::{DoesEvent, DoesProcess, EventResult, app_state::AppState, animation::ColorAnimation, data::Vector2, error::Error};

pub(crate) type HotspotCallback<'a> = Box<dyn FnMut(&AppState) + 'a>;
pub(crate) type HotspotHandle = usize;

pub(crate) struct Hotspot<'a> {
    pub position:   Vector2<i32>,
    pub size:       Vector2<u32>,

    pub callback_hover_enter:  Option<HotspotCallback<'a>>,
    pub callback_hover_leave:  Option<HotspotCallback<'a>>,
    pub callback_click:  Option<HotspotCallback<'a>>,

    pub z_index:    i32,

    pub color_aniamtion: Option<ColorAnimation>,
    pub color:           Color,

    current_color:       Color,
    current_overlapped:  bool,

}

pub(crate) struct HotspotProcessor<'a> {
    _hotspot_id: HotspotHandle,
    hotspots: HashMap<HotspotHandle, Hotspot<'a>>,
}

impl<'a> HotspotProcessor<'a> {

    pub fn new() -> Self {
        HotspotProcessor {
            _hotspot_id: 0,
            hotspots: HashMap::new()
        }
    }

    pub fn create_hotspot<'b>(&mut self, color: Color, z_index: i32) -> HotspotHandle {
        let handle = self._hotspot_id;
        self._hotspot_id = self._hotspot_id + 1;

        let output = Hotspot {
            position:  Vector2::zero(),
            size:      Vector2::zero(),

            callback_click:  None,
            callback_hover_enter:  None,
            callback_hover_leave:  None,

            z_index,
            color_aniamtion: None,
            color,

            current_color: Color::RGBA(0,0,0,0),
            current_overlapped: false
        };

        self.hotspots.insert(handle, output);
        handle
    }

    pub fn get_hotspot(&self, handle: HotspotHandle) -> Option<&Hotspot<'a>> {
        self.hotspots.get(&handle)
    }

    pub fn get_hotspot_mut(&mut self, handle: HotspotHandle) -> Option<&mut Hotspot<'a>> {
        self.hotspots.get_mut(&handle)
    }

    pub fn remove_hotspot(&mut self, handle: HotspotHandle) -> () {
        self.hotspots.remove(&handle);
    }

}

impl DoesEvent for HotspotProcessor<'_> {

    fn event<'a>(&mut self, event: Event, canvas: &mut Canvas<Window>, app_state: &AppState<'_, '_, '_, '_>) -> Result<EventResult, Error> {

        match event {
            Event::MouseButtonDown { mouse_btn: MouseButton::Left, x, y, .. } => {
                for (_handle, hotspot) in self.hotspots.iter_mut() {
                    if hotspot.current_overlapped && hotspot.callback_click.is_some() {
                        hotspot.callback_click.as_mut().unwrap()(app_state);
                    }
                }
            },

            _ => ()
        }

        Ok(EventResult::Continue)
    }

}

impl<'queue, 'texture> DoesProcess<'texture, 'queue> for HotspotProcessor<'_> {

    fn process<'a>(&mut self, canvas: &mut Canvas<Window>, app_state: &'a AppState<'_, 'texture, '_, 'queue>) -> Result<(), Error> {

        // Update hover

        for (_handle, hotspot) in self.hotspots.iter_mut() {

            // Hotspot coordinates are expected to be relative to window.

            let mouse_state = app_state.mouse_state.expect("No MouseState.");
            let mouse_position = Vector2::new(mouse_state.x(), mouse_state.y());

            let was_overlapped = hotspot.current_overlapped;

            let overlaps = crate::utils::position_size_overlaps(hotspot.position, hotspot.size, mouse_position);

            hotspot.current_overlapped = overlaps;

            if !was_overlapped && overlaps {
                hotspot.color_aniamtion = Some(ColorAnimation::new(0.2, hotspot.current_color, hotspot.color));
                if hotspot.callback_hover_enter.is_some() {

                    // @note HA! you can't edit editor state from here, cause the RefCell borrowing will panic during run time! ?????????????? NOT AN &mut editor nearby that would cause this.

                    // @note The reasoning of the comment above was based upon assumption that what I am editing right now is part of Editor impl, which is not, and no other references to Editor nor other parts of AppState RefCells should exist longer than a processor is using them.

                    // TAKE A BREAK! Let the different apporach sink in so that you don't kill yourself.

                    hotspot.callback_hover_enter.as_mut().unwrap()(app_state);
                }
            } else if was_overlapped && !overlaps {
                hotspot.color_aniamtion = Some(ColorAnimation::new(0.2, hotspot.current_color, Color::RGBA(0,0,0,0)));

                if hotspot.callback_hover_leave.is_some() {

                    // @note same as enter...

                    hotspot.callback_hover_leave.as_mut().unwrap()(app_state);
                }
            }

        }

        // Render hotspots

        const ORDER_HOTSPOT_DEFAULT: i32 = 0;

        let mut render_queue = app_state.render_queue.borrow_mut();

        for (handle, hotspot) in self.hotspots.iter_mut() {
            let position = hotspot.position;
            let size = hotspot.size;

            if hotspot.color_aniamtion.is_some() {
                let color_animation = hotspot.color_aniamtion.as_mut().unwrap();

                hotspot.current_color = color_animation.process(0.01667);

                if color_animation.is_finished() {
                    hotspot.color_aniamtion = None;
                }
            }

            let color = hotspot.current_color;

            render_queue.enqueue(ORDER_HOTSPOT_DEFAULT + hotspot.z_index, Box::new(move |canvas, _app_state|
            {
                canvas.set_draw_color(color);
                canvas.fill_rect(Rect::new(position.x, position.y, size.x, size.y)).unwrap();
            }));

        }

        Ok(())
    }

}
