# meme'D [meme editor]

Simple utility for creating memes.

Written in Rust in a naive way without using any GUI framework. The application introduces some abstractions for GUI related stuff, but none of them are really thought through. You have been warned.

## Features:

 - listing of templates
 - ability to add multiple blocks of text to an image
 - colors
 - copy to clipboard (windows only for now)

## Usage

Copy meme templates in to the /templates directory, inside the application working directory.

Select meme by clicking on it.

Add text block by clicking anywhere in the editor space.

## Used technology

 - clipboard-win
 - stretch
 - image-rs
 - sdl2 + sdl2 image + sdl2 ttf

by Daniel Jenikovský
